import React from 'react'
import { Router, Link } from '@reach/router'
import FirstPage from './pages/first-page'
import SecondPage from './pages/second-page'
import logo from './logo.svg'
import './App.css'

function App() {
  return (
    <div className="App">
      <div>
        <nav>
          <Link to="/">Home</Link>
          {' | '}
          <Link to="/first">First Page</Link>
          {' | '}
          <Link to="/second">Second Page</Link>
        </nav>
        <Router>
          <FirstPage path="/" />
          <FirstPage path="/first" />
          <SecondPage path="/second" />
        </Router>
      </div>
    </div>
  )
}

export default App
